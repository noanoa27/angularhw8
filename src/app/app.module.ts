import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { WelcomeComponent } from './welcome/welcome.component';

//firabase modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { environment } from '../environments/environment';
import { Routes, RouterModule } from '@angular/router';

//import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { LoginComponent } from './login/login.component';
import { NavComponent } from './nav/nav.component';


@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    WelcomeComponent,
    LoginComponent,
    NavComponent,
 
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
   // AngularFontAwesomeModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    RouterModule.forRoot([
      //{ path:'', component: SignupComponent },
      { path:'welcome', component: WelcomeComponent },
     { path:'signup', component: SignupComponent },
      { path:'login', component: LoginComponent },
      { path:'**', component: SignupComponent }
    ])
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
