import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import {Observable} from 'rxjs'; //לטעון את המחלקה אובסרוובל מתוך המחלקה rxjs
import {AngularFireDatabase,AngularFireList} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  register(email:string, password:string)
  {
    return this.fireBaseAuth
              .auth
              .createUserWithEmailAndPassword(email, password);
  }

  login(email:string,password:string){
    return this.fireBaseAuth.auth.signInWithEmailAndPassword(email,password)
  }

  logout(){
    return this.fireBaseAuth.auth.signOut();
  }
  
  updateProfile(user, name:string)
  {
    user.updateProfile({displayName:name, photoURL:''});
  }

  isAuth()
  {
    return this.fireBaseAuth.authState.pipe(map(auth => auth));
  }

  addUser(user,name:string,email:string){
    let uid = user.uid;
    let ref = this.db.database.ref('/'); //האנד פוינט הראשי של דטה בייס
    ref.child('users').child(uid).push({name:name, email: email});  //יוצרת אנד פוינט חדש
  }
 
  user:Observable<firebase.User>; //היוזר מסוג אובסרבבל

  constructor(private fireBaseAuth:AngularFireAuth,
              private db:AngularFireDatabase) { 
  this.user=fireBaseAuth.authState;
  }
  
}
