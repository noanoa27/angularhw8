// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAovOj4MXIe7nDqXEN_-Ki56ktfmgXQK4g",
    authDomain: "fbreg-77e83.firebaseapp.com",
    databaseURL: "https://fbreg-77e83.firebaseio.com",
    projectId: "fbreg-77e83",
    storageBucket: "fbreg-77e83.appspot.com",
    messagingSenderId: "735660958498"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
