import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { ActivatedRoute } from "@angular/router";
import { TodosService } from '../todos.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import {AngularFireDatabase,AngularFireList} from '@angular/fire/database';


@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  name = '';
  user;
  isLogged = false;
 
  todos = [ ];
 // todoTextFromTodo="no text so far"
  text:string;
  
 addTodo(){
   this.todosService.addTodo(this.text);
   this.text = '';
 }
  
  constructor(private db: AngularFireDatabase,
    public authService: AuthService, 
    private route: ActivatedRoute,private router:Router,
     public afAuth: AngularFireAuth,
     private todosService:TodosService) 
  {
    this.afAuth.user.subscribe(userInfo=>{
      if(this.authService.isAuth())
      {
        this.user = userInfo;
      }});
   }

   ngOnInit() {
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(
        todos =>{
          this.todos=[];
          todos.forEach(
            todo =>{
              let y = todo.payload.toJSON();
              y["$key"] = todo.key;
              this.todos.push(y);
            }
          )
        }
      )
    })

  }


  onLogout()
  {
    console.log("logout");
    this.afAuth.auth.signOut();
    this.router.navigate(['/signup']);
  }

}
